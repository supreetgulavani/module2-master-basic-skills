# Face Detection 

Face detection is a method used to identify faces of humans or animals in images or videos.
Face detection can be widely used for surveillance purposes, entertainment industry, etc.
There are various types of methods that can be used to implement face detection:

** 1. Feature based** : Done by extracting features of the face.

** 2. Knowledge based** : Done on the basis of calculating the distance between eyes, nose, lips, etc.

** 3. Template Matching**: Done by using predefined templates which have been detected by edge detection method. It is very inaccurate though.

**4. Appearance Based**: Done by training a lot of face images to find out the characteristics that would help to accurately detect faces. Algorithms like PCA, SVM, HMM, MRF, etc used.

### Some modules that can be used for face detection are Haar Cascades, Dlib Frontal Face detector, MTCNN, DNN face detector in OpenCV, etc.

