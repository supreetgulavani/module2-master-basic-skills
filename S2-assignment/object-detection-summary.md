# Object Detection

Object detection is a technology in the computer vision domain that helps detect objects in the images and videos.
Methods for object detection are either machine learning-based approaches or deep learning-based approaches.

For ML approaches, it becomes necessary to first define features and then using a technique for eg support vector machines (SVM) for the classification. (SIFT, HOG, etc.)
Deep learning techniques are able to do end-to-end object detection without specifically defining features, and are typically based on convolutional neural networks (CNN).

### Types are two step object detection, one step object detection, Heatmap based object detection. 

#### Interpreting the object localisation can be done in various ways which includes creating a box around the object or marking every pixel in the image which contains the object, also known as segmentation. 

