 # Face Landmarks

As we all know our face has some distinct features that makes us different from the other people. These are known as landmarks.
These landmarks are typically eyes, ears, nose, eyebrows, mouth, jawline. 

The co-ordinates of these landmarks are first calculated using various frameworks/detectors.
The co-ordinates of every face would be different thus making it easy to detect faces in future.
Although, this requires a lot of computational power and dataset. 

The 106p face landmark detection is basically plotting 106 points on the entire face to get more accurate results.
