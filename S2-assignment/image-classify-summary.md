# Image Classification

Image Classification is domain where deep neural networks come into action.
The most famous image Classification is via CNN (Convolutional Neural networks)

## What is CNN:
CNN is a deep neural network based algorithm which is widely used for image realted applications.
It has one input layer, one output layer and multiple hidden layers. The convolution operation is carried out in this algorithm and thus the name.

## Layers

**1. Convolutional**: The input image is broken down to pixels. This layer basically compares those pixels abstracting the output to a feature map 

**2. RELU**: Based on the mathematical function Rectified Linear Unit, this layer acts as an activation function and activates a node only if the input is greater than zero. 
 
**3. Pooling**: The pooling layer takes small rectangular blocks from the Convolutional layer and sub samples it to produce a single output from that  block: max, average, etc.

**4. Fully Connected**: Connects all the previous layers to produce a final output.

A diagram of CNN flow:
 ![CNN Flow](cnn.png)
