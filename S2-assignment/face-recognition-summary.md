# Face Recognition

Face Recognition is a method to identify a person using their face. It can be used to recognize people from images, videos, live camera feeds, etc.

## How does Face Recognition work:

Face Recognition uses various algorithms to specify differet features on the person's face like the distance between the eyes, shape of the chin, distance of the lips, etc. A mathematical model of this data is prepared so as to easily compute and detect faces.

A "false negative" is when the model fails to match the person to the image which is fed as a database.
A "false positive" is when the model matches a person's face but the match is incorrrect. 

FaceNet is a very popular model for Face Recognition.
